<?php
require_once 'header.php';
require_once 'db_connect.php';
require_once 'Publication.Class.php';
require_once 'Article.Class.php';
require_once 'News.Class.php';
require_once 'PublicationsWriter.Class.php';


try {
    $articles = new PublicationsWriter('article', $pdo);
    $news = new PublicationsWriter('news', $pdo);
} catch (Exception $e) {
    echo $error = ' MESSAGE: ' . $e->getMessage() . '<br> FILE: ' . $e->getFile();
}

include 'blog.html.php';
require_once 'footer.php';