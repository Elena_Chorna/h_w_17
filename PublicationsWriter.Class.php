<?php

require_once 'db_connect.php';
require_once 'Publication.Class.php';
require_once 'News.Class.php';
require_once 'Article.Class.php';
/*
 * Create class PublicationsWriter
 *
 */

class PublicationsWriter {
   public $publications = array();
   public function __construct($publicationType, PDO $pdo){
       if($publicationType != 'article' && $publicationType != 'news'){
           throw new Exception('Incorrect publication type');
       }
       $query = "SELECT * FROM articles WHERE type =:publicationType";
       $stmt = $pdo->prepare($query);
       $stmt->bindValue(':publicationType', $publicationType);
       $stmt->execute();
       $rows = $stmt->fetchAll();
       if(empty($rows)) {
           return null;
       }
       foreach ($rows as $row) {
           if ($row['type'] == 'article') {
               $this->publications[] = new Article(
                   $row['id'],
                   $row['title'],
                   $row['short_content'],
                   $row['content'],
                   $row['type'],
                   $row['author']
               );
           } else if ($row['type'] == 'news') {
               $this->publications[] = new News(
                   $row['id'],
                   $row['title'],
                   $row['short_content'],
                   $row['content'],
                   $row['type'],
                   $row['source']
               );
           }
       }
   }
}
