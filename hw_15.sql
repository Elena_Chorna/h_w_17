-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Трв 12 2017 р., 18:07
-- Версія сервера: 5.5.53
-- Версія PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `hw_15`
--

-- --------------------------------------------------------

--
-- Структура таблиці `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_content` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `author` text,
  `source` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `articles`
--

INSERT INTO `articles` (`id`, `title`, `short_content`, `content`, `type`, `author`, `source`) VALUES
(1, 'News 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet rhoncus nisi, eu egestas urna ullamcorper vel. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet rhoncus nisi, eu egestas urna ullamcorper vel. Ut porta ligula ut dui sagittis hendrerit. Praesent metus diam, laoreet ac mi vitae, pharetra posuere odio. Maecenas tristique gravida libero quis pretium. Phasellus sit amet quam leo. Nam in nisi elit. Integer ut lectus euismod, tempor magna sit amet, vulputate dolor. Aenean ultricies tortor in turpis eleifend scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet rhoncus nisi, eu egestas urna ullamcorper vel. Ut porta ligula ut dui sagittis hendrerit. Praesent metus diam, laoreet ac mi vitae, pharetra posuere odio. Maecenas tristique gravida libero quis pretium. Phasellus sit amet quam leo. Nam in nisi elit. Integer ut lectus euismod, tempor magna sit amet, vulputate dolor. Aenean ultricies tortor in turpis eleifend scelerisque.', 'news', NULL, 'google.com'),
(2, 'News 2', 'Nam in nisi elit. Integer ut lectus euismod, tempor magna sit amet, vulputate dolor. Aenean ultricies tortor in turpis eleifend scelerisque. ', 'Quisque pretium elementum accumsan. Nullam tempus neque id lacinia consectetur. Donec eget facilisis urna, cursus varius nulla. Donec blandit massa id tellus dignissim convallis at ut nulla. Praesent id diam ut ipsum fermentum eleifend quis vitae lectus. Phasellus at bibendum risus, et ultricies nulla. In lacinia efficitur metus et gravida. Morbi vehicula blandit eleifend. Nullam semper a sem sit amet sollicitudin. Sed vitae nibh ultricies, dignissim turpis id, accumsan lacus. Vivamus elementum, leo sed pulvinar sodales, mi felis consectetur sapien, quis aliquet justo justo quis elit.', 'news', NULL, 'yandex.ru'),
(3, 'Article 1', 'Nulla eleifend laoreet commodo. Vivamus nibh nisl, cursus eget dictum nec, pellentesque eget massa.', 'Nulla eleifend laoreet commodo. Vivamus nibh nisl, cursus eget dictum nec, pellentesque eget massa. Cras nec nibh efficitur, cursus massa ut, blandit nisi. Nullam eget scelerisque leo, a pharetra libero. In pellentesque quam ligula. Proin aliquam, lacus sit amet dictum vulputate, sem ligula dignissim turpis, sit amet porta sapien tortor ullamcorper quam. Ut at ex in ante tincidunt laoreet.\r\nNulla eleifend laoreet commodo. Vivamus nibh nisl, cursus eget dictum nec, pellentesque eget massa. Cras nec nibh efficitur, cursus massa ut, blandit nisi. Nullam eget scelerisque leo, a pharetra libero. In pellentesque quam ligula. Proin aliquam, lacus sit amet dictum vulputate, sem ligula dignissim turpis, sit amet porta sapien tortor ullamcorper quam. Ut at ex in ante tincidunt laoreet.', 'article', 'Author 1', NULL),
(4, 'Article 2', 'Duis turpis lectus, tincidunt eu auctor et, sagittis vitae risus. Nulla vehicula iaculis magna vel rutrum.', 'Duis in est felis. Suspendisse aliquet in neque sit amet vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sagittis in ex et condimentum. Praesent tincidunt sagittis est eget iaculis. Donec nunc orci, varius sit amet arcu vitae, pharetra fermentum sem. Duis turpis lectus, tincidunt eu auctor et, sagittis vitae risus. Nulla vehicula iaculis magna vel rutrum.\r\nDuis in est felis. Suspendisse aliquet in neque sit amet vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sagittis in ex et condimentum. Praesent tincidunt sagittis est eget iaculis. Donec nunc orci, varius sit amet arcu vitae, pharetra fermentum sem. Duis turpis lectus, tincidunt eu auctor et, sagittis vitae risus. Nulla vehicula iaculis magna vel rutrum.', 'article', 'Author 2', NULL);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
