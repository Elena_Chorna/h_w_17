<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2> All Articles</h2>
                <?php
                foreach ($articles->publications as $publication) {
                    echo $publication -> getShortPreview();
                }; ?>
            </div>

            <div class="col-md-6">
                <h2> All News</h2>
                <?php
                foreach ($news->publications as $publication) {
                    echo $publication -> getShortPreview();
                }; ?>
            </div>
        </div>
    </div>
</section>


