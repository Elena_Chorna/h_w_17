<?php

/*
 * Create class Publication
 */
abstract class Publication{
    protected $id;
    protected $title;
    protected $shortContent;
    protected $content;
    protected $type;

    public function getId(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getContent(){
        return $this->content;
    }
    public function getShortContent(){
        return $this->shortContent;
    }
    public function getType(){
        return $this->type;
    }
    /*
     * Constructor
     */
    public function __construct($id, $title, $shortContent, $content, $type){
        $this->id = $id;
        $this->title = $title;
        $this->shortContent = $shortContent;
        $this->content = $content;
        $this->type = $type;
    }

    public function setID($id){
        $this->id = $id;
    }

    /*
   * Static Create
   */
    public static function create($id, $pdo){
        if($id <= 0){
            throw new Exception('Incorrect ID');
        }
        if(!is_a($pdo, 'PDO')){
            throw new Exception('Not connected to the database server.');
        }


        $query = "SELECT * FROM articles WHERE id=:id";
        $stmt = $pdo->prepare($query);
        $stmt -> bindValue(':id', $id);
        $stmt -> execute();
        $row = $stmt->fetchObject();
        if (empty($row)) {
            return null;
        }
        $publication = '';
        if($row->type == 'article'){
            $publication = new Article(
                $row->id,
                $row->title,
                $row->short_content,
                $row->content,
                $row->type,
                $row->author
            );
        }else if($row->type == 'news'){
            $publication = new News(
                $row->id,
                $row->title,
                $row->short_content,
                $row->content,
                $row->type,
                $row->source
            );
        }
        $publication->setID($row->id);
        return $publication;
    }
}