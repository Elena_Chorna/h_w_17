<?php
/*
 * Create class Article
 */
class Article extends Publication {
    protected $author;

    public function getAuthor(){
        return $this->author;
    }
    /*
     * Constructor
     */
    public function __construct($id, $title, $shortContent, $content, $type, $author){
        parent::__construct($id, $title, $shortContent, $content, $type);
        $this->author = $author;
    }
    public function getShortPreview(){
        $str = '<h3>Title: ' . $this->getTitle() . '</h3>';
        $str .= '<h4>Author: ' . $this->getAuthor() . '</h4>';
        $str .= '<p>' . $this -> getShortContent(). '</p>';
        $str .= '<p><a href="publication.php?id=' . $this->getId() . '" >Read More</a></p>';
        return $str;
    }
}