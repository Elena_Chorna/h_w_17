<?php
/*
 * Create class News
 */
class News extends Publication {
    protected $source;
    public function getSource(){
        return $this->source;
    }
    /*
     * Constructor
     */
    public function __construct($id, $title, $shortContent, $content, $type, $source){
        parent::__construct($id, $title, $shortContent, $content, $type);
        $this->source = $source;
    }
    public function getShortPreview(){
        $str = '<h3>Title: ' . $this->getTitle() . '</h3>';
        $str .= '<h4>Source: ' . $this->getSource() . '</h4>';
        $str .= '<p>' . $this -> getShortContent(). '</p>';
        $str .= '<p><a href="publication.php?id=' . $this->getId() . '" >Read More</a></p>';
        return $str;
    }
}