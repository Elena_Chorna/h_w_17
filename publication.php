<?php
require_once 'header.php';
require_once 'db_connect.php';
require_once 'Publication.Class.php';
require_once 'Article.Class.php';
require_once 'News.Class.php';


$id = '';
if(isset($_GET['id'])){
    $id = $_GET['id'];
}

try {
    $publication = Publication::create($id, $pdo);
} catch (Exception $e) {
    echo $error = ' MESSAGE: ' . $e->getMessage() . '<br> FILE: ' . $e->getFile();
}
include 'publication.html.php';
require_once 'footer.php';