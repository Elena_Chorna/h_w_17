<?php

try{
	$pdo = new PDO('mysql:host=localhost;dbname=hw_15','root','');
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$pdo->exec("SET NAMES 'utf8'");
	
}catch(PDOException $e){
	echo "Не получилось подключиться к Базе Данных.<br>";
	echo $e->getMessage();
	exit();
}
